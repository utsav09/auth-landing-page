/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    spacing: {
      0: '0rem',
      0.5: '0.2rem',
      1: '0.4rem',
      1.5: '0.6rem',
      2: '0.8rem',
      3: '1.2rem',
      4: '1.6rem',
      5: '2rem',
      6: '2.4rem',
      7: '2.8rem',
      8: '3.2rem',
      9: '3.6rem',
      10: '4rem',
      11: '4.4rem',
      12: '4.8rem',
      14: '5.6rem',
      15: '6rem',
      16: '6.4rem'
    },
    colors: {
      darkBlue: '#0f172a',
      white: '#ffffff',
      lightGray: "#cbd5e1",
      midGray: '#94a3b8',
      offWhite: "#e3e8ef",
      purple: "#a855f7",
      transparent: "#00000000",
    },
    fontSize: {
      xs: '1.3rem',
      sm: '1.4rem',
      md: '1.6rem',
      lg: ['1.8rem', 1.3],
      xl: ['2.2rem', 1.3],
      '2xl': ['2.4rem'],
      '3xl': ['2.6rem'],
      '4xl': ['3.2rem'],
      '5xl': ['3.6rem', 1.1],
      '6xl': ['4.4rem', 1],
      '7xl': ['4.8rem', 1],
      '8xl': ['5.6rem', 1],
      '9xl': ['8rem', 1],
    },
    fontFamily: {
      inter: 'var(--font-inter)',
      'space-grotesk': 'var(--font-spaceGrotesk)'
    },
    backgroundImage: {
      primaryButton: 'linear-gradient(rgb(168, 85, 247), rgb(168, 85, 247)), linear-gradient(rgb(168, 85, 247), rgb(233, 213, 255) 75%, transparent 100%)',
      primaryButtonInner: 'linear-gradient(#a855f7,#a855f7) padding-box,linear-gradient(#a855f7,#e9d5ff 75%,transparent 100%) border-box',
      sliderBlur: 'linear-gradient(var(--direction), rgb(15, 23, 42), rgba(15, 23, 42, 0))'
    },
    keyframes: {
      fadeIn: {
        from: { opacity: 0, transform: "translateY(-10px)" },
        to: { opacity: 1, transform: "none" }
      },
      primarySlider: {
        from: { left: '0%' },
        to: { left: '-100%' }
      },
      secondarySlider: {
        from: { left: '100%' },
        to: { left: '0%' }
      },
      gridMotion: {
        from: { transform: "translateY(0)" },
        to: { transform: "translateY(-245px)" }
      },
      pulse: {
        '0%': {
          opacity: 0,
          transform: "scale(0.25) translateZ(0)"
        },
        '30%': {
          opacity: 0.4
        },
        '70%': {
          opacity: 0
        },
        '80%': {
          transform: "scale(1) translateZ(0)"
        },
        '100%': {
          transform: "scale(1) translateZ(0)"

        }
      }
    },
    animation: {
      fadeIn: "fadeIn 1000ms var(--animation-delay, 0ms) ease forwards",
      primarySlider: "primarySlider 25s linear infinite",
      secondarySlider: "secondarySlider 25s linear infinite",
      gridMotion: "gridMotion 15s linear infinite",
      pulse: "pulse 12s var(--animation-delay, 0ms) linear infinite"
    }
  },
  plugins: [],
}
