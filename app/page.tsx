import { Container } from './component/container';
import { Clients } from './component/section/clients';
import { Features } from './component/section/features';
import { Hero } from './component/section/homepage-hero';

export default function Home() {
  return (
    <div className='w-screen overflow-x-hidden'>
      <Container>
        <Hero />
        <Clients />
        <Features />
      </Container>
    </div>
  );
}
