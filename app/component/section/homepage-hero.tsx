import { Button } from '../button';
import { HeroButton, HeroSubTitle, HeroTitle, HeroWrapper } from '../hero';
import { MagicWandIcon } from '../icons/magic';
import { ChevronRight } from '../icons/right';
import { HeroGradient } from '../illustrations/hero-bg';

export const Hero = () => {
  return (
    <div className='relative mx-auto'>
      <HeroWrapper>
        <HeroButton className='z-10 mb-6 animate-fadeIn opacity-0 [--animation-delay:400ms]'>
          <span className='inline-flex items-center font-space-grotesk'>
            Skillmine Auth is now live in beta
            <ChevronRight className='-mr-2 ml-1 h-4 w-4' />
          </span>
        </HeroButton>
        <HeroTitle className='animate-fadeIn opacity-0 [--animation-delay:600ms]'>
          <>
            More than user authentication. <br />
            Complete User Management
          </>
        </HeroTitle>
        <HeroSubTitle className='mb-8 animate-fadeIn opacity-0 [--animation-delay:1000ms]'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae
          provident magni libero delectus ut, laborum iure at. Voluptatum a
          illum.
        </HeroSubTitle>

        <Button
          variant='secondary'
          size='medium'
          className='animate-fadeIn opacity-0 [--animation-delay:1400ms]'
          border='normal'
        >
          <>
            <MagicWandIcon className='mr-2 h-5 w-5' />
            Get Started
          </>
        </Button>
      </HeroWrapper>

      <div className='absolute inset-0 -z-10 -ml-[12rem] -mr-[12rem] overflow-hidden rounded-b-[4.8rem]'>
        <div className='absolute bottom-0 left-1/2 -translate-x-1/2'>
          <HeroGradient />
        </div>
      </div>
    </div>
  );
};
