import classNames from 'classnames';
import { Grid } from '../grid';
import { ShieldIcon } from '../icons/shield';
import { HaloEffect } from '../illustrations/halo';

export const Features = () => {
  return (
    <div className='relative'>
      <div className='grid grid-cols-2 pb-[8rem] pt-[20rem]'>
        <div className='font-inter'>
          <p className='mb-2 text-sm font-medium text-midGray'>
            The security first platform
          </p>
          <p className='text-gradient mb-4 font-space-grotesk text-6xl font-bold text-offWhite'>
            Simplify your security with authentication services
          </p>
          <p className='text-md text-midGray'>
            Define access roles for the end-users, and extend your authorization
            capabilities to implement dynamic access control.
          </p>
        </div>
        <div className='mx-auto w-1/2'>
          <div className='relative -mt-1 py-1.5'>
            <div className='flex items-center justify-center'>
              <div className='relative flex aspect-square w-[19.2rem] items-center justify-center'>
                <HaloEffect className='absolute inset-0 left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 blur-lg' />
                <Grid />
                <div>
                  <div
                    className={classNames(
                      'grid-logo-bg relative flex h-16 w-16 rotate-[-14deg] items-center justify-center rounded-3xl border border-transparent',
                      'rounded-3xl before:absolute before:inset-0 before:bg-[#1e293b4d]'
                    )}
                  >
                    <ShieldIcon className='h-8 w-8 text-offWhite' />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='absolute inset-0 -z-10 overflow-hidden'>
        <div className='absolute left-1/2 top-0 flex aspect-square w-1/2 -translate-x-1/2 -translate-y-1/2 items-center justify-center'>
          <div className='absolute inset-0 rounded-full bg-purple opacity-50  blur-[120px]' />
        </div>
      </div>
    </div>
  );
};
