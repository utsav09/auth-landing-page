import classNames from 'classnames';
import { AirbnbLogo } from '../logos/airbnb';
import { CadburyLogo } from '../logos/cadbury';
import { CanonLogo } from '../logos/canon';
import { FacebookLogo } from '../logos/facebook';
import { QuoraLogo } from '../logos/quora';
import { SparkLogo } from '../logos/spark';
import { TinderLogo } from '../logos/tinder';

const clientList = [
  AirbnbLogo,
  CadburyLogo,
  CanonLogo,
  FacebookLogo,
  QuoraLogo,
  SparkLogo,
  TinderLogo,
];

export const Clients = () => {
  return (
    <div
      className={classNames(
        'relative w-full overflow-hidden py-16 text-md text-offWhite',
        'before:pointer-events-none before:absolute before:inset-0 before:z-10 before:w-[12.8rem] before:bg-sliderBlur before:[--direction:to_right]',
        'after:pointer-events-none after:absolute after:inset-0 after:left-auto after:z-10 after:w-[12.8rem] after:bg-sliderBlur after:[--direction:to_left]'
      )}
    >
      <div className='relative h-16 w-full overflow-x-hidden px-1'>
        <div className='absolute left-0 top-0 flex w-full animate-primarySlider items-center justify-around'>
          {clientList.map((Client, idx) => (
            <Client key={idx} />
          ))}
        </div>
        <div className='absolute left-0 top-0 flex w-full animate-secondarySlider items-center justify-around'>
          {clientList.map((Client, idx) => (
            <Client key={idx} />
          ))}
        </div>
      </div>
    </div>
  );
};
