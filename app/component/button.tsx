import { cva, VariantProps } from 'class-variance-authority';
import Link from 'next/link';
import { AnchorHTMLAttributes, ButtonHTMLAttributes } from 'react';

type ButtonBaseProps = VariantProps<typeof buttonClasses> & {
  children: React.ReactNode;
};

interface ButtonAsButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  href?: never;
}

interface ButtonAsAnchorProps extends AnchorHTMLAttributes<HTMLAnchorElement> {
  href: string;
}

type ButtonProps = ButtonBaseProps &
  (ButtonAsAnchorProps | ButtonAsButtonProps);

const buttonClasses = cva(
  'inline-flex justify-between items-center font-space-grotesk',
  {
    variants: {
      variant: {
        primary: 'bg-[#0f172a40] text-lightGray hover:text-white',
        secondary: 'bg-offWhite text-darkBlue',
      },
      size: {
        small: 'px-3 py-0.5 text-sm',
        medium: 'px-4 py-1.5 text-md font-medium',
      },
      border: {
        rounded: 'rounded-full',
        normal: 'rounded-xl',
      },
    },
    defaultVariants: {
      variant: 'primary',
      size: 'small',
      border: 'rounded',
    },
  }
);

export const Button = ({
  variant,
  size,
  className,
  children,
  border,
  ...props
}: ButtonProps) => {
  const classes = buttonClasses({ variant, size, className, border });

  if ('href' in props && props.href !== undefined) {
    return (
      <Link {...props} className={classes}>
        {children}
      </Link>
    );
  }

  return (
    <button {...props} className={classes}>
      {children}
    </button>
  );
};
