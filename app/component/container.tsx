import classNames from 'classnames';

export const Container = ({
  className,
  children,
}: {
  className?: string;
  children: React.ReactNode;
}) => {
  return (
    <div
      className={classNames('mx-auto max-w-[120rem] px-4 md:px-8', className)}
    >
      {children}
    </div>
  );
};
