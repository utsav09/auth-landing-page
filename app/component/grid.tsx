export const Grid = () => {
  return (
    <div className='halo-mask pointer-events-none absolute left-1/2 top-1/2 h-[50rem] w-[50rem] -translate-x-1/2 -translate-y-1/2 overflow-hidden rounded-full'>
      <div className='h-[200%] animate-gridMotion'>
        <div className='grid1 absolute inset-0 opacity-20 blur-sm' />
        <div className='grid2 absolute inset-0' />
        <div className='grid3 absolute inset-0 opacity-20 blur-sm' />
        <div className='grid4 absolute inset-0' />
      </div>
    </div>
  );
};
