import classnames from 'classnames';
import { Button } from './button';

interface HeroText {
  className?: string;
  children: React.ReactNode;
}

export const HeroTitle = ({ className, children }: HeroText) => (
  <h1
    className={classnames(
      'text-gradient pb-4 font-bold text-8xl font-space-grotesk',
      className
    )}
  >
    {children}
  </h1>
);

export const HeroSubTitle = ({ className, children }: HeroText) => (
  <p
    className={classnames(
      'text-lightGray px-[16.8rem] mx-auto text-lg font-space-grotesk',
      className
    )}
  >
    {children}
  </p>
);

export const HeroWrapper = ({ className, children }: HeroText) => (
  <div
    className={classnames('pt-[20.8rem] pb-[12.8rem] text-center', className)}
  >
    {children}
  </div>
);

export const HeroButton = ({ children, className }: HeroText) => (
  <div
    className={classnames(
      'inline-flex relative',
      'before:absolute before:inset-0 before:bg-purple before:blur-md',
      className
    )}
  >
    <div className='primaryButton rounded-full text-sm hover:text-white relative border border-transparent'>
      <button className='bg-[#1e293b80] transition-colors ease duration-300 text-lightGray hover:text-white px-3 py-0.5 rounded-full inline-flex items-center'>
        {children}
      </button>
    </div>
  </div>
);
