import classNames from 'classnames';
import { Button } from './button';
import { Container } from './container';
import { ChevronDownIcon } from './icons/down';
import { LockIcon } from './icons/lock';
import { ShieldIcon } from './icons/shield';

export const Header = () => {
  return (
    <div className='fixed left-0 top-0 z-20 w-full font-space-grotesk'>
      <Container className='flex flex-col items-center'>
        <ul
          className={classNames(
            'navBackground relative mx-auto mt-4 flex items-center justify-between gap-5 rounded-2xl border border-transparent px-3 py-1',
            'before:pointer-events-none before:absolute before:inset-0 before:rounded-2xl before:bg-[#1e293b4d]'
          )}
        >
          <li className='group'>
            <ShieldIcon className='h-8 w-8 text-purple' />
          </li>
          <li className='group'>
            <div className='flex items-center gap-1 text-md text-white'>
              Product
              <ChevronDownIcon className='mt-1 h-3 w-3 transition-transform duration-300 ease-in-out group-hover:rotate-180' />
            </div>
            <div className='invisible absolute left-0 w-full group-hover:visible'>
              <div
                className={classNames(
                  'navBackground invisible relative mt-4 w-full rounded-2xl border border-transparent px-3 pb-5 pt-2  group-hover:visible',
                  '-translate-y-2 opacity-0 transition-[opacity,transform] duration-500 ease-in-out group-hover:translate-y-0 group-hover:opacity-100',
                  'before:absolute before:inset-0 before:rounded-2xl before:bg-[#1e293b4d]'
                )}
              >
                <div className='grid w-full grid-cols-2 gap-6'>
                  <div className='text-md'>
                    <p className='mb-4 font-semibold uppercase text-white'>
                      Platform
                    </p>
                    <div className='flex flex-col gap-7'>
                      <div className='flex items-center gap-3'>
                        <LockIcon />
                        <p className='text-offWhite'>Access Management</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <LockIcon />
                        <p className='text-offWhite'>Access Management</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <LockIcon />
                        <p className='text-offWhite'>Access Management</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <LockIcon />
                        <p className='text-offWhite'>Access Management</p>
                      </div>
                    </div>
                  </div>
                  <div className='text-md'>
                    <p className='mb-4 font-semibold uppercase text-white'>
                      Features
                    </p>
                    <div className='flex flex-col space-y-5'>
                      <div className='flex items-center gap-3'>
                        <p className='text-offWhite'>Single Sign On</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <p className='text-offWhite'>Universal Login</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <p className='text-offWhite'>
                          Multifactor Authentication
                        </p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <p className='text-offWhite'>Action</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <p className='text-offWhite'>Password Breach</p>
                      </div>
                      <div className='flex items-center gap-3'>
                        <p className='text-offWhite'>Passwordless Login</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li className='group'>
            <div className='fl text-md text-white'>Pricing</div>
            <div className='invisible absolute border border-purple text-md text-white group-hover:visible'>
              This is the hover text
            </div>
          </li>
          <li className='group'>
            <div className='text-md text-white'>Product</div>
            <div className='invisible absolute border border-purple text-md text-white group-hover:visible'>
              This is the hover text
            </div>
          </li>
          <li className='group'>
            <div className='text-md text-white'>Features</div>
            <div className='invisible absolute border border-purple text-md text-white group-hover:visible'>
              This is the hover text
            </div>
          </li>
          <li className='group'>
            <div className='text-md text-white'>Use Case</div>
            <div className='invisible absolute border border-purple text-md text-white group-hover:visible'>
              This is the hover text
            </div>
          </li>
          <li className='group'>
            <div className='text-md text-white'>Company</div>
            <div className='invisible absolute border border-purple text-md text-white group-hover:visible'>
              This is the hover text
            </div>
          </li>
          <li className='group'>
            <Button
              variant='secondary'
              size='medium'
              border='normal'
              className='-mr-2'
            >
              Get Started
            </Button>
          </li>
        </ul>
      </Container>
    </div>
  );
};
